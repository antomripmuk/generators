package distribution;

import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.SparseDoubleMatrix2D;
import cern.colt.matrix.linalg.Algebra;

import java.util.List;


public class NonCorrelatedMultivariateNormalDistribution {
  private List<Double> mean;
  private DoubleMatrix2D covariance;

  private static Algebra algebra = new Algebra();
  private MultivariateNormalDistribution distribution = new MultivariateNormalDistribution();

  public NonCorrelatedMultivariateNormalDistribution() { }

  public NonCorrelatedMultivariateNormalDistribution(List<Double> mean, List<Double> covariance) {
    this.mean = mean;
    this.covariance = getDiagonalMatrix(covariance);

    this.distribution.setMean(mean);
    this.distribution.setCovariance(this.covariance);
  }

  public List<Double> getMean() {
    return mean;
  }

  public Double getMean(int i) {
    return mean.get(i);
  }

  public void setMean(List<Double> mean) {
    this.mean = mean;
    this.distribution.setMean(mean);
  }

  public DoubleMatrix2D getCovariance() {
    return covariance;
  }

  public void setCovariance(List<Double> covariance) {
    this.covariance = getDiagonalMatrix(covariance);
    this.distribution.setCovariance(this.covariance);
  }

  public double density (List<Double> x) {
    return distribution.density(x);
  }

  public DoubleMatrix2D getCorrelation() {
    return distribution.getCorrelation();
  }

  private DoubleMatrix2D getDiagonalMatrix(List<Double> v) {
    double[][] values = new double[v.size()][v.size()];

    for (int i = 0; i < v.size(); i++) {
      values[i][i] = v.get(i);
    }

    return new SparseDoubleMatrix2D(values);
  }

}
