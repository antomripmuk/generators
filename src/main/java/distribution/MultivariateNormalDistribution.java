package distribution;

import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import cern.colt.matrix.linalg.Algebra;

import java.util.List;


public class MultivariateNormalDistribution implements MultivariateDistribution {
  private List<Double> mean;
  private DoubleMatrix2D covariance;
  private DoubleMatrix2D inverseCovariance;

  private static Algebra algebra = new Algebra();

  public MultivariateNormalDistribution() { }

  public MultivariateNormalDistribution(List<Double> mean, DoubleMatrix2D covariance) {
    this.mean = mean;
    this.covariance = covariance;
    this.inverseCovariance = algebra.inverse(covariance);
  }

  public List<Double> getMean() {
    return mean;
  }

  public Double getMean(int i) {
    return mean.get(i);
  }

  public void setMean(List<Double> mean) {
    this.mean = mean;
  }

  public DoubleMatrix2D getCovariance() {
    return covariance;
  }

  public void setCovariance(DoubleMatrix2D covariance) {
    this.covariance = covariance;
    this.inverseCovariance = algebra.inverse(covariance);
  }

  public int getDimension() {
    return mean.size();
  }

  public double density (List<Double> x) {
    double sum = 0.0;
    double[] temp = new double[mean.size()];

    for (int i = 0; i < mean.size(); i++) {
      sum = 0.0;

      for (int j = 0; j < mean.size(); j++) {
        sum += ((x.get(j) - mean.get(j)) * inverseCovariance.getQuick(j, i));
      }

      temp[i] = sum;
    }

    sum = 0.0;

    for (int i = 0; i < mean.size(); i++) {
      sum += temp[i] * (x.get(i) - mean.get(i));
    }

    return (Math.exp(-0.5 * sum) / Math.sqrt (Math.pow (2 * Math.PI, mean.size()) * algebra.det(covariance)));
  }

  public DoubleMatrix2D getCorrelation () {
    //DoubleMatrix2D correlation = new DenseDoubleMatrix2D()
    double corr[][] = new double[mean.size()][mean.size()];

    for (int i = 0; i < mean.size(); i++) {
      for (int j = 0; j < mean.size(); j++)
        corr[i][j] = - covariance.get(i, j) / Math.sqrt (covariance.get(i, i) * covariance.get(j, j));
      corr[i][i] = 1.0;
    }

    return new DenseDoubleMatrix2D(corr);
  }


  public static double density(List<Double> mean, DoubleMatrix2D covariance, List<Double> x) {
    double sum = 0.0;
    DoubleMatrix2D inverseCovariance;

    if (covariance.columns() != covariance.rows()) {
      throw new IllegalArgumentException ("Covariance matrix must be a square matrix");
    }
    if (mean.size() != covariance.columns()) {
      throw new IllegalArgumentException ("mu and sigma must have the same dimension");
    }

    inverseCovariance = algebra.inverse(covariance);

    double[] temp = new double[mean.size()];
    for (int i = 0; i < mean.size(); i++)
    {
      sum = 0.0;

      for (int j = 0; j < mean.size(); j++) {
        sum += ((x.get(j) - mean.get(j)) * inverseCovariance.getQuick(j, i));
      }

      temp[i] = sum;
    }

    sum = 0.0;

    for (int i = 0; i < mean.size(); i++) {
      sum += temp[i] * (x.get(i) - mean.get(i));
    }

    return (Math.exp(-0.5 * sum) / Math.sqrt (Math.pow (2 * Math.PI, mean.size()) * algebra.det(covariance)));
  }

}
