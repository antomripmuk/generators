package distribution;

import util.ContinuousDistributionUtil;
import util.NumericUtil;


public abstract class ContinuousDistribution implements Distribution {
  protected static int decPrec = 15;
  protected static final double XBIG = 100.0;
  protected static final double XBIGM = 1000.0;

  public abstract double density (double x);

  public double barF (double x) {
    return 1.0 - cdf (x);
  }

  private void findInterval (double u, double [] iv) {
    if (u > 1.0 || u < 0.0) {
      throw new IllegalArgumentException ("u not in [0, 1]");
    }

    final double XLIM =  Double.MAX_VALUE/2.0;
    final double B0 = 8.0;
    double b = B0;

    while (b < XLIM && u > cdf(b)) {
      b *= 2.0;
    }
    if (b > B0) {
      iv[0] = b / 2.0;
      iv[1] = Math.min(b, Double.POSITIVE_INFINITY);
      return;
    }

    double a = -B0;
    while (a > -XLIM && u < cdf(a))
      a *= 2.0;
    if (a < -B0) {
      iv[1] = a/2.0;
      iv[0] = Math.max (a, Double.NEGATIVE_INFINITY);
      return;
    }
    iv[0] = Math.max (a, Double.NEGATIVE_INFINITY);
    iv[1] = Math.min (b, Double.POSITIVE_INFINITY);
  }

  public double inverseBrent (double a, double b, double u, double tol)  {
    if (u > 1.0 || u < 0.0)
      throw new IllegalArgumentException ("u not in [0, 1]");
    if (b < a) {
      double ctemp = a;   a = b;   b = ctemp;
    }
    if (u <= 0.0) {
      return Double.NEGATIVE_INFINITY;
    }
    if (u >= 1.0) {
      return Double.POSITIVE_INFINITY;
    }

    final int MAXITER = 50;
    tol += ContinuousDistributionUtil.EPSARRAY[decPrec] + NumericUtil.DBL_EPSILON;
    double ua = cdf(a) - u;

    if (ua > 0.0) {
      throw new IllegalArgumentException ("u < cdf(a)");
    }

    double ub = cdf(b) - u;

    if (ub < 0.0) {
      throw new IllegalArgumentException ("u > cdf(b)");
    }

    // Initialize
    double c = a;
    double uc = ua;
    double len = b - a;
    double t = len;

    if (Math.abs(uc) < Math.abs(ub)) {
      a = b; b = c; c = a;
      ua = ub; ub = uc; uc = ua;
    }

    int i;

    for (i = 0; i < MAXITER; ++i) {
      double tol1 = tol + 4.0*NumericUtil.DBL_EPSILON*Math.abs(b);
      double xm = 0.5*(c - b);

      if (Math.abs(ub) == 0.0 || (Math.abs(xm) <= tol1)) {
        if (b <= Double.NEGATIVE_INFINITY) return Double.NEGATIVE_INFINITY;
        if (b >= Double.POSITIVE_INFINITY) return Double.POSITIVE_INFINITY;
        return b;
      }

      double s, p, q, r;
      if ((Math.abs(t) >= tol1) && (Math.abs(ua) > Math.abs(ub))) {
        if (a == c) {
          // linear interpolation
          s = ub/ua;
          q = 1.0 - s;
          p = 2.0 * xm * s;
        } else {
          // quadratic interpolation
          q = ua / uc;
          r = ub / uc;
          s = ub / ua;
          p = s*(2.0*xm*q*(q - r) - (b - a)*(r - 1.0));
          q = (q - 1.0)*(r - 1.0)* (s - 1.0);
        }
        if (p > 0.0)
          q = -q;
        p = Math.abs(p);

        // Accept interpolation?
        if ((2.0*p >= (3.0*xm*q - Math.abs(q*tol1))) ||
            (p >= Math.abs(0.5*t*q))) {
          len = xm;
          t = len;
        } else {
          t = len;
          len = p/q;
        }

      } else {
        len = xm;
        t = len;
      }

      a = b;
      ua = ub;
      if (Math.abs(len) > tol1)
        b += len;
      else if (xm < 0.0)
        b -= tol1;
      else
        b += tol1;
      ub = cdf(b) - u;

      if (ub*(uc/Math.abs(uc)) > 0.0) {
        c = a;
        uc = ua;
        len = b - a;
        t = len;
      } else if (Math.abs(uc) < Math.abs(ub)) {
        a = b; b = c; c = a;
        ua = ub; ub = uc; uc = ua;
      }
    }

    return b;
  }

  public double inverseBisection (double u) {
    final int MAXITER = 100;              // Maximum number of iterations
    final double EPSILON = ContinuousDistributionUtil.EPSARRAY[decPrec];  // Absolute precision
    final double XLIM =  Double.MAX_VALUE/2.0;
    final String lineSep = System.getProperty("line.separator");

    if (u > 1.0 || u < 0.0) {
      throw new IllegalArgumentException ("u not in [0, 1]");
    }
    if (decPrec > NumericUtil.DBL_DIG) {
      throw new IllegalArgumentException ("decPrec too large");
    }
    if (decPrec <= 0) {
      throw new IllegalArgumentException ("decPrec <= 0");
    }

    double x = 0.0;
    if (u <= 0.0) {
      return Double.NEGATIVE_INFINITY;
    }
    if (u >= 1.0) {
      return Double.POSITIVE_INFINITY;
    }

    double [] iv = new double [2];
    findInterval (u, iv);
    double xa = iv[0];
    double xb = iv[1];
    double yb = cdf(xb) - u;
    double ya = cdf(xa) - u;
    double y;

    boolean fini = false;
    int i = 0;
    while (!fini) {

      x = (xa + xb)/2.0;
      y = cdf(x) - u;
      if ((y == 0.0) ||
          (Math.abs ((xb - xa)/(x + NumericUtil.DBL_EPSILON)) <= EPSILON))  {
        fini = true;
      }
      else if (y*ya < 0.0)
        xb = x;
      else
        xa = x;
      ++i;

      if (i > MAXITER) {
        fini = true;
      }
    }
    return x;
  }

  public double inverseF (double u) {
    double [] iv = new double [2];
    findInterval (u, iv);
    return inverseBrent (iv[0], iv[1], u, ContinuousDistributionUtil.EPSARRAY[decPrec]);
  }

  public double getMean() {
    throw new UnsupportedOperationException("getMean is not implemented");
  }

  public double getVariance() {
    throw new UnsupportedOperationException("getVariance is not implemented");
  }

  public double getStandardDeviation() {
    throw new UnsupportedOperationException ("getStandardDeviation is not implemented");
  }

  public double getXinf() {
    return Double.NEGATIVE_INFINITY;
  }

  public double getXsup() {
    return Double.POSITIVE_INFINITY;
  }

}
