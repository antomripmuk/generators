package distribution;


public interface Distribution {

  public double cdf (double x);

  public double barF (double x);

  public double inverseF (double u);

  public double getMean();

  public double getVariance();

  public double getStandardDeviation();

}
