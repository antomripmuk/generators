package distribution;

import util.NormalDistributionUtil;
import util.NumericUtil;


public class NormalDistribution extends ContinuousDistribution {
  protected double mean = 0.0;
  protected double standardDeviation = 1.0;

  public NormalDistribution (double mean, double standardDeviation) {
    this.mean = mean;
    this.standardDeviation = standardDeviation;
  }

  public double density (double x) {
    double z = (x - mean) / standardDeviation;
    return Math.exp (-0.5*z*z) / (NormalDistributionUtil.RAC2PI * standardDeviation);
  }

  public double cdf (double x) {
    return cdf01 ((x - mean) / standardDeviation);
  }

  public double barF (double x) {
    return barF01 ((x - mean) / standardDeviation);
  }

  public double inverseF (double u) {
    return mean + standardDeviation * inverseF01 (u);
  }

  public double getMean() {
    return NormalDistribution.getMean (mean, standardDeviation);
  }

  public double getCovariance() {
    return standardDeviation;
  }

  public double getVariance() {
    return NormalDistribution.getVariance (mean, standardDeviation);
  }

  public double getStandardDeviation() {
    return NormalDistribution.getStandardDeviation (mean, standardDeviation);
  }

  public static double density01 (double x)  {
    return Math.exp(-0.5 * x * x) / NormalDistributionUtil.RAC2PI;
  }

  public static double density (double mean, double standardDeviation, double x)  {
    if (standardDeviation <= 0) {
      throw new IllegalArgumentException ("covariance <= 0");
    }

    double z = (x - mean) / standardDeviation;
    return Math.exp (-0.5 * z * z) / (NormalDistributionUtil.RAC2PI * standardDeviation);
  }

  public static double cdf01 (double x) {
   /*
    * Returns P[X < x] for the normal distribution.
    * As in J. L. Schonfelder, Math. of Computation, Vol. 32,
    * pp 1232--1240, (1978).
    */

    double t, r;

    if (x <= -XBIG) {
      return 0.0;
    }
    if (x >= XBIG) {
      return 1.0;
    }

    x = -x/ NumericUtil.SQRT2;

    if (x < 0) {
      x = -x;
      t = (x - 3.75) / (x + 3.75);
      r = 1.0 -
          0.5 *
          Math.exp (-x * x) *
          NumericUtil.evalChebychevPolynomial(NormalDistributionUtil.NORMAL2_A, NormalDistributionUtil.COEFFMAX, t);
    } else {
      t = (x - 3.75) / (x + 3.75);
      r = 0.5 *
          Math.exp (-x * x) *
          NumericUtil.evalChebychevPolynomial(NormalDistributionUtil.NORMAL2_A, NormalDistributionUtil.COEFFMAX, t);
    }
    return r;
  }

  public static double cdf (double mean, double standardDeviation, double x) {
    if (standardDeviation <= 0) {
      throw new IllegalArgumentException ("covariance <= 0");
    }

    return cdf01 ((x - mean) / standardDeviation);
  }

  public static double barF01 (double x) {
   /*
    * Returns P[X >= x] = 1 - F (x) where F is the normal distribution by
    * computing the complementary distribution directly; it is thus more
    * precise in the tail.
    */

    final double KK = 5.30330085889910643300;      // 3.75 Sqrt (2)
    double y, t;
    int neg;

    if (x >= XBIG) {
      return 0.0;
    }
    if (x <= -XBIG) {
      return 1.0;
    }

    if (x >= 0.0) {
      neg = 0;
    }
    else {
      neg = 1;
      x = -x;
    }

    t = (x - KK) / (x + KK);
    y = NumericUtil.evalChebychevPolynomial(NormalDistributionUtil.AbarF, 24, t);
    y = y * Math.exp (-x * x / 2.0) / 2.0;

    if (neg == 1) {
      return 1.0 - y;
    }

    return y;
  }

  public static double barF (double mean, double standardDeviation, double x) {
    if (standardDeviation <= 0) {
      throw new IllegalArgumentException ("covariance <= 0");
    }

    return barF01 ((x - mean) / standardDeviation);
  }

  public static double inverseF01 (double u) {
       /*
        * Returns the inverse of the cdf of the normal distribution.
        * Rational approximations giving 16 decimals of precision.
        * J.M. Blair, C.A. Edwards, J.H. Johnson, "Rational Chebyshev
        * approximations for the Inverse of the Error Function", in
        * Mathematics of Computation, Vol. 30, 136, pp 827, (1976)
        */

    int i;
    boolean negative;
    double y, z, v, w;
    double x = u;

    if (u < 0.0 || u > 1.0)
      throw new IllegalArgumentException ("u is not in [0, 1]");
    if (u <= 0.0)
      return Double.NEGATIVE_INFINITY;
    if (u >= 1.0)
      return Double.POSITIVE_INFINITY;

    // Transform x as argument of InvErf
    x = 2.0 * x - 1.0;
    if (x < 0.0) {
      x = -x;
      negative = true;
    }
    else {
      negative = false;
    }

    if (x <= 0.75) {
      y = x * x - 0.5625;
      v = w = 0.0;

      for (i = 6; i >= 0; i--) {
        v = v * y + NormalDistributionUtil.InvP1[i];
        w = w * y + NormalDistributionUtil.InvQ1[i];
      }
      z = (v / w) * x;
    }
    else if (x <= 0.9375) {
      y = x * x - 0.87890625;
      v = w = 0.0;

      for (i = 7; i >= 0; i--) {
        v = v * y + NormalDistributionUtil.InvP2[i];
        w = w * y + NormalDistributionUtil.InvQ2[i];
      }

      z = (v / w) * x;
    }
    else {
      if (u > 0.5) {
        y = 1.0 / Math.sqrt (-Math.log (1.0 - x));
      }
      else {
        y = 1.0 / Math.sqrt (-Math.log (2.0 * u));
      }

      v = 0.0;

      for (i = 10; i >= 0; i--) {
        v = v * y + NormalDistributionUtil.InvP3[i];
      }

      w = 0.0;

      for (i = 8; i >= 0; i--) {
        w = w * y + NormalDistributionUtil.InvQ3[i];
      }

      z = (v / w) / y;
    }

    if (negative) {
      if (u < 1.0e-105) {
        w = Math.exp (-z * z) / NormalDistributionUtil.RACPI;  // pdf
        y = 2.0 * z * z;
        v = 1.0;

        double term = 1.0;

        // Asymptotic series for erfc(z) (apart from exp factor)
        for (i = 0; i < 6; ++i) {
          term *= -(2 * i + 1) / y;
          v += term;
        }

        // Apply 1 iteration of Newton solver to get last few decimals
        z -= u / w - 0.5 * v / z;
      }

      return -(z * NumericUtil.SQRT2);
    }

    return z * NumericUtil.SQRT2;
  }

  public static double inverseF (double mu, double sigma, double u)  {
    if (sigma <= 0)
      throw new IllegalArgumentException ("sigma <= 0");
    return mu + sigma * inverseF01 (u);
  }

  public static double[] getMLE (double[] x, int n) {
    if (n <= 0)
      throw new IllegalArgumentException ("n <= 0");

    double[] parameters = new double[2];
    double sum = 0.0;
    for (int i = 0; i < n; i++)
      sum += x[i];
    parameters[0] = sum / n;

    sum = 0.0;
    for (int i = 0; i < n; i++)
      sum = sum + (x[i] - parameters[0]) * (x[i] - parameters[0]);
    parameters[1] = Math.sqrt (sum / n);

    return parameters;
  }

  public static NormalDistribution getInstanceFromMLE (double[] x, int n) {
    double parameters[] = getMLE (x, n);
    return new NormalDistribution (parameters[0], parameters[1]);
  }

  public static double getMean (double mean, double standardDeviation) {
    if (standardDeviation <= 0.0) {
      throw new IllegalArgumentException ("covariance <= 0");
    }

    return mean;
  }

  public static double getVariance (double mean, double standardDeviation) {
    if (standardDeviation <= 0.0) {
      throw new IllegalArgumentException ("sigma <= 0");
    }

    return (standardDeviation * standardDeviation);
  }

  public static double getStandardDeviation (double mean, double standardDeviation) {
    return standardDeviation;
  }

  public String toString () {
    return getClass().getSimpleName() + " : mu = " + mean + ", sigma = " + standardDeviation;
  }
}
