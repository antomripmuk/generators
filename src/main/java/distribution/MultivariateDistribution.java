package distribution;

import cern.colt.matrix.DoubleMatrix2D;

import java.util.List;


public interface MultivariateDistribution {

  public double density (List<Double> x);

  public List<Double> getMean();

  public DoubleMatrix2D getCovariance();

  public DoubleMatrix2D getCorrelation();

}
