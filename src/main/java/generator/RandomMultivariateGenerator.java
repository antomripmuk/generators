package generator;

import cern.colt.list.DoubleArrayList;
import umontreal.iro.lecuyer.rng.RandomStream;

import java.util.ArrayList;
import java.util.List;

public abstract class RandomMultivariateGenerator {
  protected RandomStream stream;
  protected RandomVariateGenerator generator;

  abstract public DoubleArrayList nextDouble();

  public List<Double> getArrayList(int n) {

    // sanity check
    if (n <= 0) {
      throw new IllegalArgumentException ("n must be positive.");
    }

    // create list
    ArrayList list = new ArrayList(n);

    for (int i = 0; i < n; i++) {
      list.add(nextDouble());
    }

    return list;
  }

  public RandomStream getStream()  {
    if (null != generator) {
      return generator.getStream();
    }

    return stream;
  }

  public void setStream (RandomStream stream) {
    if (null != generator) {
      generator.setStream(stream);
    }
    else {
      this.stream = stream;
    }
  }
}
