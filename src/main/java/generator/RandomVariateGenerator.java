package generator;


import distribution.Distribution;
import umontreal.iro.lecuyer.rng.RandomStream;

import java.util.ArrayList;
import java.util.List;


public class RandomVariateGenerator {
  protected RandomStream stream;
  protected Distribution dist;

  public RandomVariateGenerator() {}

  public RandomVariateGenerator (RandomStream s, Distribution dist)  {
    this.stream = s;
    this.dist = dist;
  }

  public RandomStream getStream() {
    return stream;
  }

  public void setStream (RandomStream stream) {
    this.stream = stream;
  }

  public Distribution getDistribution() {
    return dist;
  }

  public void setDistribution(Distribution dist) {
    this.dist = dist;
  }

  public double nextDouble()  {
    return dist.inverseF (stream.nextDouble());
  }

  public List<Double> getArrayList(int n) {

    // sanity check
    if (n <= 0) {
      throw new IllegalArgumentException ("n must be positive.");
    }

    // create list
    List<Double> list = new ArrayList<Double>(n);

    for (int i = 0; i < n; i++) {
      list.add(nextDouble());
    }

    return list;
  }

  public String toString () {
    if (dist != null)
      return getClass().getSimpleName() + " with  " + dist.toString();
    else
      return getClass().getSimpleName() ;
  }
}
