package generator;


import cern.colt.list.DoubleArrayList;
import cern.colt.matrix.DoubleMatrix2D;

import distribution.NormalDistribution;

import java.util.List;


public class MultivariateNormalGenerator extends RandomMultivariateGenerator {
  protected List<Double> mean;
  protected DoubleMatrix2D covariance;
  protected DoubleMatrix2D sqrtCovariance;
  protected double[] temp;

  public MultivariateNormalGenerator(
      NormalVariateGenerator generator,
      List<Double> mean,
      DoubleMatrix2D covariance)
  {
    init(generator, mean);
    this.covariance = covariance;
  }

  private void init(NormalVariateGenerator generator, List<Double> mean) {
    if (generator == null) {
      throw new NullPointerException("gen1 is null");
    }

    NormalDistribution dist = (NormalDistribution) generator.getDistribution();

    if (dist.getMean() != 0.0) {
      throw new IllegalArgumentException("mu != 0");
    }
    if (dist.getCovariance() != 1.0) {
      throw new IllegalArgumentException("sigma != 1");
    }

    this.generator = generator;
    this.mean = mean;
    temp = new double[mean.size()];
  }

  public List<Double> getMean() {
    return mean;
  }

  public double getMean(int i) {
    return mean.get(i);
  }

  public void setMean(List<Double> mean) {
    if (mean.size() != this.mean.size()) {
      throw new IllegalArgumentException("Incompatible length of mean vector");
    }

    this.mean = mean;
  }

  public void setMean(int i, double mean) {
    if (this.mean.size() < i) {
      throw new IllegalArgumentException("Index too big");
    }

    this.mean.set(i, mean);
  }

  public DoubleMatrix2D getSigma() {
    return covariance;
  }


  public DoubleArrayList nextDouble() {
      DoubleArrayList p = new DoubleArrayList(mean.size());

    for (int i = 0; i < mean.size(); i++) {
      temp[i] = generator.nextDouble();  
      p.add(0.0);
    }

    for (int i = 0; i < mean.size(); i++) {
      for (int c = 0; c < mean.size(); c++) {
        p.set(i, (p.get(i) + covariance.getQuick(i, c) * temp[c]));
      }

      p.set(i, (p.get(i) + mean.get(i)));
    }

    return p;
  }
}
