package generator;

import distribution.NormalDistribution;
import umontreal.iro.lecuyer.rng.RandomStream;

public class NormalVariateGenerator extends RandomVariateGenerator {
  protected double mean;
  protected double covariance = -1.0;

  public NormalVariateGenerator (RandomStream s)  {
    this (s, 0.0, 1.0);
  }

  public NormalVariateGenerator (RandomStream s, double mean, double covariance)  {
    super (s, new NormalDistribution(mean, covariance));
    setParams (mean, covariance);
  }

  public NormalVariateGenerator (RandomStream s, NormalDistribution dist)  {
    super (s, dist);

    if (dist != null) {
      setParams (dist.getMean(), dist.getCovariance());
    }
  }

  public static double nextDouble (RandomStream s, double mean, double covariance)  {
    return NormalDistribution.inverseF(mean, covariance, s.nextDouble());
  }

  public double getMean() {
    return mean;
  }

  public double getCovariance() {
    return covariance;
  }

  protected void setParams (double mean, double covariance) {
    if (covariance <= 0) {
      throw new IllegalArgumentException ("covariance <= 0");
    }

    this.mean = mean;
    this.covariance = covariance;
  }
}
