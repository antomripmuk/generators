package data;


import cern.colt.list.ObjectArrayList;
import util.JSONUtil;

public class SubsetResponse {
    ObjectArrayList records;

  public SubsetResponse() { }

  public SubsetResponse(ObjectArrayList records) {
    this.records = records;
  }

  public ObjectArrayList getRecords() {
    return records;
  }

  public void setRecords(ObjectArrayList records) {
    this.records = records;
  }

  @Override
  public String toString() {
    return JSONUtil.writeObject(this);
  }
}
