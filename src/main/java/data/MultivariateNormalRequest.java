package data;

import util.JSONUtil;

import java.util.List;


public class MultivariateNormalRequest {
  private List<Double> mean;
  private double[][] covariance;

  public MultivariateNormalRequest() { }

  public List<Double> getMean() {
    return mean;
  }

  public void setMean(List<Double> mean) {
    this.mean = mean;
  }

  public double[][] getCovariance() {
    return covariance;
  }

  public void setCovariance(double[][] covariance) {
    this.covariance = covariance;
  }

  @Override
  public String toString() {
    return JSONUtil.writeObject(this);
  }
}
