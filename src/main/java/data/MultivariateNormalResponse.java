package data;



import cern.colt.list.DoubleArrayList;
import util.JSONUtil;

import java.util.List;


public class MultivariateNormalResponse {
  List<DoubleArrayList> records;

  public MultivariateNormalResponse() { }

  public MultivariateNormalResponse(List<DoubleArrayList> records) {
    this.records = records;
  }

  public List<DoubleArrayList> getRecords() {
    return records;
  }

  public void setRecords(List<DoubleArrayList> records) {
    this.records = records;
  }

  @Override
  public String toString() {
    return JSONUtil.writeObject(this);
  }
}
