package data;

import util.JSONUtil;

public class ErrorMessage {
  private String error;

  public ErrorMessage() { }

  public ErrorMessage(String message) { }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

  @Override
  public String toString() {
    return JSONUtil.writeObject(this);
  }
}
