package data;

import util.JSONUtil;

import java.util.List;

public class SubsetRequest {
  List<List<Double>> records;

  public SubsetRequest() { }

  public List<List<Double>> getRecords() {
    return records;
  }

  public void setRecords(List<List<Double>> records) {
    this.records = records;
  }

  @Override
  public String toString() {
    return JSONUtil.writeObject(this);
  }
}
