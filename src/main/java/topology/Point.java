package topology;


import cern.colt.list.DoubleArrayList;
import util.JSONUtil;

/**
 * Created with IntelliJ IDEA.
 * User: shushumiga
 * Date: 4/19/15
 * Time: 20:22
 * To change this template use File | Settings | File Templates.
 */
public class Point {

   private DoubleArrayList points;

    public Point(DoubleArrayList points) {
        this.points = points;
    }

    public double getPointDistance(Point point) {
        double sum = 0.0;
        DoubleArrayList pointList = point.getPoints();
        for(int i = 0; i < points.size(); i++) {
            sum += (pointList.get(i) - points.get(i)) * (pointList.get(i) - points.get(i));
        }
//        return Math.sqrt(sum);
        return sum;
    }

    public DoubleArrayList getPoints() {
        return points;
    }

    public int getDim() {
        return points.size();
    }

    public void setPoints(DoubleArrayList points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return points.toString();
    }

    @Override
    public boolean equals(Object o) {
        boolean equals = true;
        Point point = (Point) o;
        DoubleArrayList pointList = point.getPoints();
        for (int i = 0; i < points.size(); i++) {
            equals = points.get(i) == pointList.get(i) && equals;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        for (Double coordinates : points.elements()) {
            result = prime * result + coordinates.hashCode();
        }
        return result;
    }
}
