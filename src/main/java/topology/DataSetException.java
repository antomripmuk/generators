package topology;


public class DataSetException extends RuntimeException {

  public DataSetException(Throwable cause) {
    super(cause);
  }

  public DataSetException(String message) {
    super(message);
  }

  public DataSetException(String message, Throwable cause) {
    super(message, cause);
  }
}
