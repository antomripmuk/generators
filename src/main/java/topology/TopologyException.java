package topology;

public class TopologyException extends RuntimeException {

  public TopologyException(Throwable cause) {
    super(cause);
  }

  public TopologyException(String message) {
    super(message);
  }

  public TopologyException(String message, Throwable cause) {
    super(message, cause);
  }

}
