package topology;


import cern.colt.list.DoubleArrayList;
import cern.colt.list.ObjectArrayList;
import util.JSONUtil;


public class DataSet {
    private Integer dim = 0;
    private ObjectArrayList dataPoints = new ObjectArrayList();

    public DataSet(int dim) {
        this.dim = dim;
    }

    public DataSet(ObjectArrayList dataPoints) {
        if (dataPoints != null) {
            this.dim = ((Point)dataPoints.get(0)).getDim();
            this.dataPoints = dataPoints;
        }
    }

    public Integer getDim() {
        return dim;
    }

    public void setDim(Integer dim) {
        this.dim = dim;
    }

    /**
     * Get data point
     *
     * @param row
     * @return
     */
    public Point get(int row) {
        return (Point)dataPoints.getQuick(row);
    }

    public int getSize() {
        return dataPoints.size();
    }

    public boolean contains(Point point) {
        return dataPoints.contains(point, true);
    }

//    /**
//     * Get a new data point built from column of all data points
//     *
//     * @param column
//     * @return
//     */
//    public List<Double> getColumn(final int column) {
//        final List<Double> columnData = new ArrayList<Double>(dataPoints.size());
//
//        if (column >= dim) {
//            throw new DataSetException("Out of boundaries, requested" + column + ", but size is " + dim);
//        }
//
//        for(Point row: dataPoints) {
//            columnData.add(row.getPoints().get(column));
//        }
//
//        return columnData;
//    }

//    /**
//     * Get median of data point
//     *
//     * @param row
//     * @return
//     */
//    public Double getMedian(int row) {
//        if (row >= dataPoints.size()) {
//            throw new DataSetException("Out of boundaries, requested" + row + ", but size is " + dataPoints.size());
//        }
//
//        // sort a copy
//        DoubleArrayList rowCopy = new ArrayList<Double>(dataPoints.get(row).getPoints());
//        Collections.sort(rowCopy);
//
//        return MathUtil.getMedian(rowCopy);
//    }

//    /**
//     * Get median a new data point built from column of all data points
//     *
//     * @param column
//     * @return
//     */
//    public Double getColumnMedian(int column) {
//        if (column >= dim) {
//            throw new DataSetException("Out of boundaries, requested" + column + ", but size is " + dim);
//        }
//
//        List<Double> columnData = getColumn(column);
//        Collections.sort(columnData);
//
//        return MathUtil.getMedian(columnData);
//    }

//    /**
//     * Get data point of medians of all column medians
//     *
//     * @return
//     */
//    public List<Double> getCoordinatewiseMedian() {
//        List<Double> median = new ArrayList<Double>(dim);
//
//        for (int i = 0; i < dim; i++) {
//            List<Double> column = getColumn(i);
//
//            Collections.sort(column);
//            median.add(MathUtil.getMedian(column));
//        }
//
//        return median;
//    }

    /**
     * Get all data points
     *
     * @return
     */
    public ObjectArrayList getDataPoints() {
        return dataPoints;
    }

//    public ObjectListIterator<Point> getListIterator() {
//        return dataPoints.listIterator();
//    }

    /**
     * Add data point
     *
     * @param record
     */
    public void add(DoubleArrayList record) {
        if (record == null) {
            throw new DataSetException("Uninitialized record.");
        }
        if (record.size() != dim) {
            System.out.println("rc size: " + record.size());
            System.out.println("dim: " + dim);
            throw new DataSetException("Bad record size.");
        }

        dataPoints.add(new Point(record));
    }

    public void addAll(DataSet dataSet) {
        for (int i = 0; i < dataSet.getSize(); i++) {
            dataPoints.add(dataSet.get(i));
        }
    }

    /**
     * Remove data point
     *
     * @param pos
     */
    public void remove(int pos) {
        dataPoints.remove(pos);
    }

    /**
     * Clear all data points
     */
    public void clear() {
        dataPoints.clear();
    }

    @Override
    public String toString() {
        return JSONUtil.writeObject(dataPoints);
    }
}
