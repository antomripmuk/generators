package topology;

import util.JSONUtil;

public class Line2D {
    private double slope;
    private double intercept;
    private static Point2D point1;
    private static Point2D point2;

    public Line2D() { }

    public Line2D(Double slope, Double intercept) {
        this.slope = slope;
        this.intercept = intercept;
    }

    public static Line2D getInstance(Point2D p1, Point2D p2) {
        Line2D line = new Line2D();
        point1 = p1;
        point2 = p2;

        if (p1 == null || p2 == null) {
            throw new TopologyException("Bad data point");
        }

        line.setSlope((p1.getY() - p2.getY()) / (p1.getX() - p2.getX()));
        line.setIntercept(p1.getY() - line.getSlope() * p1.getX());

        return line;
    }

    public double getSlope() {
        return slope;
    }

    public void setSlope(double slope) {
        this.slope = slope;
    }

    public double getIntercept() {
        return intercept;
    }

    public void setIntercept(double intercept) {
        this.intercept = intercept;
    }

    public Point2D getPoint1() {
        return point1;
    }


    public Point2D getPoint2() {
        return point2;
    }

    public Double valueAt(double x) {
        return (slope * x + intercept);
    }

    @Override
    public String toString() {
        return JSONUtil.writeObject(this);
    }
}