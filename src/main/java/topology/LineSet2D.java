package topology;

import util.JSONUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class LineSet2D implements Iterator<Line2D>, Iterable<Line2D> {
  private Iterator<Line2D> iterator = null;
  private List<Line2D> lines = new ArrayList<Line2D>();

  public LineSet2D() { }

  @Override
  public Iterator<Line2D> iterator() {
    return lines.iterator();
  }

  @Override
  public boolean hasNext() {
    if (iterator.hasNext()) {
      return true;
    }

    return false;
  }

  @Override
  public Line2D next() {
    if (!iterator.hasNext()) {
      throw new NoSuchElementException();
    }

    return iterator.next();
  }

  @Override
  public void remove() {
    throw new UnsupportedOperationException();
  }

  public void add(Line2D line) {
    lines.add(line);
  }

  public void add(LineSet2D lines) {
    this.lines.addAll(lines.get());
  }

  public Line2D get(int pos) {
    return lines.get(pos);
  }

  public List<Line2D> get() {
    return lines;
  }

  public void remove(int pos) {
    lines.remove(pos);
  }

  public void clear() {
    lines.clear();
  }

  public Integer size() {
    return lines.size();
  }

  @Override
  public String toString() {
    return JSONUtil.writeObject(this);
  }
}
