package topology;

import cern.colt.list.DoubleArrayList;
import util.JSONUtil;

import java.util.*;

public class DataSet2D implements Iterator<Point2D>, Iterable<Point2D> {
  private Iterator<Point2D> iterator = null;
  private List<Point2D> data = new ArrayList<Point2D>();

  public DataSet2D() { }

  @Override
  public Iterator<Point2D> iterator() {
    return data.iterator();
  }

  @Override
  public boolean hasNext() {
    if (iterator.hasNext()) {
      return true;
    }

    return false;
  }

  @Override
  public Point2D next() {
    if (!iterator.hasNext()) {
      throw new NoSuchElementException();
    }

    return iterator.next();
  }

  @Override
  public void remove() {
    throw new UnsupportedOperationException();
  }

  public void add(Point2D point) {
    data.add(point);
  }

  public void add(DataSet2D data) {
    this.data.addAll(data.get());
  }

  public Point2D get(int pos) {
    return data.get(pos);
  }

  public List<Point2D> get() {
    return data;
  }

//  public Point2D getCoordinatewiseMedian() {
//    Point2D median = new Point2D();
//    DoubleArrayList columnX = new DoubleArrayList(data.size());
//    DoubleArrayList columnY = new DoubleArrayList(data.size());
//
//    // create X and Y column lists
//    for (Point2D point: data) {
//      columnX.add(point.getX());
//      columnY.add(point.getY());
//    }
//
//    // sort data
//    Collections.sort(columnX);
//    Collections.sort(columnY);
//
//    // add coordinatewise median x/y
//    median.setX(MathUtil.getMedian(columnX));
//    median.setY(MathUtil.getMedian(columnY));
//
//    return median;
//  }

  public void remove(int pos) {
    data.remove(pos);
  }

  public void clear() {
    data.clear();
  }

  public Integer size() {
    return data.size();
  }

  @Override
  public String toString() {
    return JSONUtil.writeObject(this);
  }
}
