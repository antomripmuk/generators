package topology;


public class DataSet2DUtil {

  public Integer smallerThan(Line2D line, DataSet2D dataSet) {
    int count = 0;
    double valueAt = 0.0;

    if (line == null) {
      throw new TopologyException("Line not initialized.");
    }
    if (dataSet == null || dataSet.size() == 0) {
      throw new TopologyException("Empty dataset.");
    }

    for(Point2D point: dataSet) {
      if ((valueAt = line.valueAt(point.getX())) < point.getY()) {
        count++;
      }
    }

    return count;
  }

  public Integer greaterThan(Line2D line, DataSet2D dataSet) {
    if (line == null) {
      throw new TopologyException("Line not initialized.");
    }
    if (dataSet == null || dataSet.size() == 0) {
      throw new TopologyException("Empty dataset.");
    }

    return dataSet.size() - smallerThan(line, dataSet);
  }
}
