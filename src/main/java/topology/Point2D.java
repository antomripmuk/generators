package topology;

import util.JSONUtil;

import java.util.LinkedList;
import java.util.List;

public class Point2D {
    private double x;
    private double y;

    public Point2D() { }

    public Point2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public boolean isEqual(Point2D point) {
        return y == point.getY() && x == point.getX();
    }

    public double getPointDistance(Point2D point2D) {
        return Math.sqrt(Math.pow((point2D.getY() - y), 2) + Math.pow((point2D.getX() - x), 2));
    }

    public List<Double> toList() {
        List<Double> coordinates = new LinkedList<Double>();
        coordinates.add(x);
        coordinates.add(y);
        return coordinates;
    }

    @Override
    public String toString() {
        return JSONUtil.writeObject(this);
    }

    @Override
    public boolean equals(Object o) {
        Point2D point = (Point2D) o;
        return x == point.getX() && y == point.getY();

    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Double.valueOf(x).hashCode();
        result = prime * result + Double.valueOf(y).hashCode();
        return result;
    }
}
