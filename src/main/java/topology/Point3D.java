package topology;

import util.JSONUtil;

import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shushumiga
 * Date: 4/19/15
 * Time: 19:02
 * To change this template use File | Settings | File Templates.
 */
public class Point3D {

    private double x;
    private double y;
    private double z;

    public Point3D() { }

    public Point3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public boolean isEqual(Point3D point) {
        return y == point.getY() && x == point.getX() && z == point.getZ();
    }

    public double getPointDistance(Point3D point3D) {
        return Math.sqrt(Math.pow((point3D.getZ() - z), 2) + Math.pow((point3D.getY() - y), 2) + Math.pow((point3D.getX() - x), 2));
    }

    public List<Double> toList() {
        List<Double> coordinates = new LinkedList<Double>();
        coordinates.add(x);
        coordinates.add(y);
        coordinates.add(z);
        return coordinates;
    }

    @Override
    public String toString() {
        return JSONUtil.writeObject(this);
    }

    @Override
    public boolean equals(Object o) {
        Point3D point = (Point3D) o;
        return x == point.getX() && y == point.getY() && z == point.getZ();

    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Double.valueOf(x).hashCode();
        result = prime * result + Double.valueOf(y).hashCode();
        result = prime * result + Double.valueOf(z).hashCode();
        return result;
    }
}
