package config;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.ConfigurationFactory;
import org.apache.commons.configuration.XMLConfiguration;


public class EaConfiguration {
  private Configuration config;

  public EaConfiguration(Configuration config) {
    this.config = config;
  }

  public static EaConfiguration readConfig(String configFile) throws ConfigurationException {
    return new EaConfiguration(new XMLConfiguration(configFile));
  }

  public Integer getPort() {
    return config.getInt("server.port");
  }
}
