package util;

public class NumericUtil {
  public static final int DBL_DIG = 15;
  public static final double SQRT2 = 1.41421356237309504880;
  public static final double DBL_EPSILON = 2.2204460492503131e-16;

  public static double evalChebychevPolynomial (double a[], int n, double x)  {
    if (Math.abs(x) > 1.0) {
      throw new IllegalArgumentException ("Chebychev polynomial evaluated at x outside [-1, 1]");
    }

    final double xx = 2.0 * x;

    double b0 = 0.0;
    double b1 = 0.0;
    double b2 = 0.0;

    for (int j = n; j >= 0; j--) {
      b2 = b1;
      b1 = b0;
      b0 = (xx * b1 - b2) + a[j];
    }

    return (b0 - b2)/2.0;
  }
}
