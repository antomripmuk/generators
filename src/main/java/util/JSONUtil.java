package util;

import org.apache.commons.lang.ClassUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.util.EnumResolver;
import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JSONUtil {
  private static final ObjectMapper jsonMapper = constructMapper();

  /**
   * Escape JSON String
   * @param s
   * @return
   */
  public static String escape(String s) {
    StringBuffer sb = new StringBuffer();

    if (s == null) {
      return "";
    }

    for (int i = 0; i < s.length(); i++) {
      char ch = s.charAt(i);
      switch (ch) {
        case '"':
          sb.append("\\\"");
          break;
        case '\\':
          sb.append("\\\\");
          break;
        case '\b':
          sb.append("\\b");
          break;
        case '\f':
          sb.append("\\f");
          break;
        case '\n':
          sb.append("\\n");
          break;
        case '\r':
          sb.append("\\r");
          break;
        case '\t':
          sb.append("\\t");
          break;
        case '/':
          sb.append("\\/");
          break;
        default:
          // Reference: http://www.unicode.org/versions/Unicode5.1.0/
          if ((ch >= '\u0000' && ch <= '\u001F') || (ch >= '\u007F' && ch <= '\u009F')
              || (ch >= '\u2000' && ch <= '\u20FF')) {
            String ss = Integer.toHexString(ch);
            sb.append("\\u");
            for (int k = 0; k < 4 - ss.length(); k++) {
              sb.append('0');
            }
            sb.append(ss.toUpperCase());
          } else {
            sb.append(ch);
          }
      }
    }

    return sb.toString();
  }

  @SuppressWarnings("unchecked")
  public static Map<String, Object> readMap(String json) {
    return readObject(json, (Class<Map<String, Object>>) (Class<?>) Map.class);
  }

  @SuppressWarnings("unchecked")
  public static Map<String, Object> readMap(JsonNode node) {
    return readObject(node, (Class<Map<String, Object>>) (Class<?>) Map.class);
  }

  @SuppressWarnings("unchecked")
  public static <T> Map<String, T> readMap(String json, Class<T> valueClass) {
    Map<String, Object> untypedMap = readMap(json);
    if (String.class.equals(valueClass) || List.class.equals(valueClass) || valueClass.isPrimitive()
        || ClassUtils.wrapperToPrimitive(valueClass) != null)
      return (Map<String, T>) untypedMap;
    Map<String, Map<String, Object>> mapOfMaps = (Map<String, Map<String, Object>>) (Map<?, ?>) untypedMap;
    Map<String, T> typedMap = new HashMap<String, T>();
    for (Map.Entry<String, Map<String, Object>> entry : mapOfMaps.entrySet()) {
      T converted = null;
      if (entry.getValue() != null) {
        if (valueClass.isInstance(entry.getValue())) {
          converted = (T) entry.getValue();
        } else {
          converted = JSONUtil.convertToObject(entry.getValue(), valueClass);
        }
      }
      typedMap.put(entry.getKey(), converted);
    }
    return typedMap;
  }

  public static <T> Map<String, T> readMap(JsonNode node, Class<T> valueClass) {
    return readMap(writeObject(node), valueClass);
  }

  public static <T> T readObject(String json, Class<T> clazz) {
    try {
      return jsonMapper.readValue(json, clazz);
    } catch (IOException e) {
      throw new IllegalStateException(e);
    }
  }

  public static <T> T readObject(String json, TypeReference<T> valueTypeRef) {
    try {
      return jsonMapper.readValue(json, valueTypeRef);
    } catch (IOException e) {
      throw new IllegalStateException(e);
    }
  }

  public static <T> T readObject(JsonNode node, Class<T> clazz) {
    try {
      return jsonMapper.treeToValue(node, clazz);
    } catch (IOException e) {
      throw new IllegalStateException(e);
    }
  }

  public static String writeObject(Object value) {
    try {
      return jsonMapper.writeValueAsString(value);
    } catch (IOException e) {
      throw new IllegalStateException(e);
    }
  }

  public static JsonNode readJsonNode(String json) {
    try {
      return jsonMapper.readTree(json);
    } catch (IOException e) {
      throw new IllegalStateException(e);
    }
  }

  public static JsonNode convertToJsonNode(Object value) {
    if (value == null)
      return null;
    return jsonMapper.valueToTree(value);
  }

  public static <T> T convertToObject(Map<String, ?> jsonObject, Class<T> clazz) {
    if (jsonObject == null)
      return null;
    JsonNode node = JSONUtil.convertToJsonNode(jsonObject);
    return JSONUtil.readObject(node, clazz);
  }

  @SuppressWarnings("unchecked")
  public static <T> Map<String, T> convertToMap(Object value) {
    if (value == null)
      return null;
    JsonNode node = JSONUtil.convertToJsonNode(value);
    return readObject(node, Map.class);
  }

  private static AnnotationIntrospector getAnnotationIntrospector() {
    return jsonMapper.getSerializationConfig().getAnnotationIntrospector();
  }

  public static <T extends Enum<T>> EnumResolver<T> constructEnumResolver(Class<T> enumClass) {
    return EnumResolver.constructFor(enumClass, getAnnotationIntrospector());
  }

  public static ObjectMapper constructMapper() {
    CustomAnnotationIntrospector annotationIntrospector = new CustomAnnotationIntrospector();

    ObjectMapper mapper = new ObjectMapper();
    SerializationConfig sCfg = mapper.getSerializationConfig().withAnnotationIntrospector(annotationIntrospector);
    mapper.setSerializationConfig(sCfg);

    DeserializationConfig dCfg = mapper.getDeserializationConfig().withAnnotationIntrospector(annotationIntrospector);
    mapper.setDeserializationConfig(dCfg);
    return mapper;
  }
}
