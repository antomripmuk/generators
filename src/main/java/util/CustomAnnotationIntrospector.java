package util;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.introspect.JacksonAnnotationIntrospector;


public class CustomAnnotationIntrospector extends JacksonAnnotationIntrospector {
  @Override
  public String findEnumValue(Enum<?> value) {
    if (value == null)
      return null;
    try {
      JsonProperty jsonProperty = value.getDeclaringClass().getField(value.name()).getAnnotation(JsonProperty.class);
      String jsonValue;
      if (jsonProperty != null) {
        jsonValue = jsonProperty.value();
      } else {
        JsonIgnore jsonIgnore = value.getDeclaringClass().getMethod("toString").getAnnotation(JsonIgnore.class);
        jsonValue = jsonIgnore != null ? value.name() : value.toString();
      }
      return jsonValue;
    } catch (NoSuchFieldException e) {
      throw new IllegalStateException(
          String.format("Value %s does not exist in enum %s", value.name(),
          value.getDeclaringClass().getSimpleName()), e
      );
    } catch (NoSuchMethodException e) {
      throw new IllegalStateException(e.getMessage(), e);
    }
  }
}
